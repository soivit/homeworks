﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Task3
{
    public delegate void EventDelegate();

    public class Cat
    {
        public event EventDelegate WakeUpEvent;

        public void Show()
        {
            Console.WriteLine("Cat wake up");
            WakeUpEvent?.Invoke();
        }
    }
}
