﻿using System;

namespace Lesson4.Task1
{
    class ClassDelegate
    {
        public delegate void Delegate2();
        public Delegate2 del2 = () => Console.WriteLine("Привет!");
    }
}
