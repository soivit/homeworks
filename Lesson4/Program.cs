﻿using System;
using Lesson4.Task1;
using Lesson4.Task2;
using Lesson4.Task3;

namespace Lesson4
{
    class Program
    {
        static void Main(string[] args)
        {
            var taskToRun = Content.Task1;

            Cat cat = new Cat();
            cat.WakeUpEvent += Mouse.Display;
            cat.WakeUpEvent += Dog.Display;
            cat.WakeUpEvent += Human.Display;
            switch (taskToRun)
            {
                case Content.Task1:
                    FirstDelegate.Show();
                    break;
                case Content.Task2:
                    GenericDelegate.Show();
                    break;
                case Content.Task3:
                    cat.Show();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
        
              
       
    }
}
