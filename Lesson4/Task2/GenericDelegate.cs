﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Task2
{
    class GenericDelegate
    {
        delegate T Delegate3<T, V>(V x);

        public static void Show()
        {
            Delegate3<string, string> del3 = (x) => "Привет " + x;
            
            Console.WriteLine(del3("Олег!"));
        }
    }
}
