﻿using System.Collections.Generic;

namespace Lesson8.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public List<Story> Stories { get; set; }
    }
}