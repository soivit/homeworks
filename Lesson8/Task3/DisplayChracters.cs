﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Lesson8.Task3
{
    class DisplayChracters
    {
        public static void Show()
        {
            var dbContext = new CharactersDbContext();
            var characters = dbContext.Characters.Include(x => x.Story.Author);

            foreach (var c in characters)
                Console.WriteLine($"FirstName: {c.FirstName}  " +
                    $"\tStory: {c.Story.Name}  \tAuthor: {c.Story.Author.FirstName} {c.Story.Author.LastName}");
          
        }
    }
}
