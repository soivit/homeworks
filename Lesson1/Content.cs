﻿namespace Lesson1
{
    enum Content
    {
        Array,
        ArrayList,
        Stack,
        Queue,
        HashTable,
        GList,
        GLinkedList,
        GStack,
        GQueue,
        GDictionary,
        GHashSet,
        DictionaryInterface,
        LinkedList
    }
}
