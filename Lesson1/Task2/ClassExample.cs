﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson1.Task2
{
    class ClassExample
    {


        public static void DictionaryInterface()
        {
            Dictionary<INumber, ICity> DicExam = new Dictionary<INumber, ICity>();
            DicExam.Add(new PostIndex("201015"), new BelarusCity("Vitebsk"));
            DicExam.Add(new PostIndex("221023"), new BelarusCity("Gomel"));
            DicExam.Add(new CityCode("745"), new BelarusCity("Moscow"));
            DicExam.Add(new CityCode("578"), new BelarusCity("Smolensk"));

            foreach (var item in DicExam)
            {
                Console.WriteLine($"Key: {item.Key.Number} Value: {item.Value.City}");
            }
        }
    }

    public class PostIndex : INumber
    {
        public string Number { get; }
        public PostIndex(string number)
        {
            Number = number;
        }
    }

    public class CityCode : INumber
    {
        public string Number { get; }
        public CityCode(string number)
        {
            Number = number;
        }
    }

    public class RussiaCity : ICity
    {
        public string City { get; }
        public RussiaCity(string city)
        {
            City = city;
        }
    }

    public class BelarusCity : ICity
    {
        public string City { get; }
        public BelarusCity(string city)
        {
            City = city;
        }
    }
}
