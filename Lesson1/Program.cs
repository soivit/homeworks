﻿using System;
using Lesson1.Task1;
using Lesson1.Task2;
using Lesson1.Task3;

namespace Lesson1
{
    class Program
    {
        static void Main(string[] args)
        {
            var taskToRun = Content.LinkedList;
            
            switch (taskToRun)
            {
                case Content.Array:
                    ArrayExample.Show();
                    break;
                case Content.ArrayList:
                    ArrayListExample.Show();
                    break;
                case Content.Stack:
                    StackExample.Show();
                    break;
                case Content.Queue:
                    QueueExample.Show();
                    break;
                case Content.HashTable:
                    HashTableExample.Show();
                    break;
                case Content.GList:
                    GListExample.Show();
                    break;
                case Content.GLinkedList:
                    GLinkedListExample.Show();
                    break;
                case Content.GStack:
                    GStackExample.Show();
                    break;
                case Content.GQueue:
                    GQueueExample.Show();
                    break;
                case Content.GDictionary:
                    GDictionaryExample.Show();
                    break;
                case Content.GHashSet:
                    GHashSetExample.Show();
                    break;
                case Content.DictionaryInterface:
                    ClassExample.DictionaryInterface();
                    break;
                case Content.LinkedList:
                    LinkedListExample.Show();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
    }
}