﻿using System;
using System.Collections;

namespace Lesson1.Task1
{
    class QueueExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  Queue \n");
            Queue queueExample = new Queue();
            queueExample.Enqueue("1");
            queueExample.Enqueue("2");
            queueExample.Enqueue("3");
            queueExample.Enqueue("4");
            queueExample.Enqueue("5");

            Console.WriteLine("Содержимое очереди:");
            foreach (var queue in queueExample)
            {
                Console.WriteLine(queue);
            }

            Console.WriteLine("\nДобавление элемента 777 в очередь");
            queueExample.Enqueue("777");
            Console.WriteLine("Содержимое очереди:");
            foreach (var queue in queueExample)
            {
                Console.WriteLine($"{queue}");
            }

            Console.WriteLine("\nУдаление элемента из очереди");
            string item2 = (string)queueExample.Dequeue();
            Console.WriteLine($"Элемент {item2} был удален");
            Console.WriteLine("Содержимое очереди:");
            foreach (var queue in queueExample)
            {
                Console.WriteLine($"{queue}");
            }
          
            Console.WriteLine("\nОтображения элемента в начале очереди без его удаления");
            Console.WriteLine($"Элемент {(string)queueExample.Peek()} находится в начале очереди");
            Console.WriteLine("Содержимое очереди:");
            foreach (var queue in queueExample)
            {
                Console.WriteLine($"{queue}");
            }
        }
    }
}
