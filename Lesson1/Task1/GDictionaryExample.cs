﻿using System;
using System.Collections.Generic;

namespace Lesson1.Task1
{
    class GDictionaryExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  Dictionary<T,V> \n");
            Dictionary<int, string> Example = new Dictionary<int, string>();
            Example.Add(1, "Понедельник");
            Example.Add(2, "Вторник");
            Example.Add(3, "Среда");
            Example.Add(4, "Четверг");
            Example.Add(5, "Пятница");
            Console.WriteLine("Содержимое словаря:");
            foreach (KeyValuePair<int, string> item in Example)
            {
                Console.WriteLine($"Key = {item.Key}, Value = {item.Value}");
            }

            Console.WriteLine("\nДобавление элемента в словарь Key=6, Value=Суббота");
            Example.Add(6, "Суббота");
            Console.WriteLine("Содержимое словаря:");
            foreach (KeyValuePair<int, string> item in Example)
            {
                Console.WriteLine($"Key = {item.Key}, Value = {item.Value}");
            }

            Console.WriteLine("\nУдаление элемента по ключу 5");
            Example.Remove(5);
            Console.WriteLine("Содержимое словаря:");
            foreach (KeyValuePair<int, string> item in Example)
            {
                Console.WriteLine($"Key = {item.Key}, Value = {item.Value}");
            }

        }
    }
}
