﻿using System;

namespace Lesson1.Task1
{
    class ArrayExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  Array \n");
            int [] arrayExample = new int[5];
            Console.WriteLine("Цикл for");
            for (int i=0; i < arrayExample.Length; i++)
            {
                Console.Write($"индекс: {i} ");
                arrayExample[i] = i + 1;
                Console.WriteLine($"элемент: {arrayExample[i]}");
            }
            Console.WriteLine("--------------------------------\n");
            
            Console.WriteLine("Цикл foreach");
            foreach (var item in arrayExample)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("--------------------------------\n");

            Console.WriteLine("Цикл while");
            var j = 0;
            while (j < arrayExample.Length)
            {
                Console.WriteLine(arrayExample[j]);
                j++;
            }
        }

    }
}
