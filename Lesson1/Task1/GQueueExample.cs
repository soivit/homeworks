﻿using System;
using System.Collections.Generic;

namespace Lesson1.Task1
{
    class GQueueExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  Queue <T>\n");
            Queue<string> Example = new Queue<string>();
            Example.Enqueue("1");
            Example.Enqueue("2");
            Example.Enqueue("3");
            Console.WriteLine("Содержимое очереди:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nДобавление элемента 777 в очередь");
            Example.Enqueue("777");
            Console.WriteLine("Содержимое очереди:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nУдаление элемента из очереди");
            string item2 = (string)Example.Dequeue();
            Console.WriteLine($"Элемент {item2} был удален");
            Console.WriteLine("Содержимое очереди:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nОтображения элемента в начале очереди без его удаления");
            Console.WriteLine($"Элемент {(string)Example.Peek()} находится в начале очереди");
            Console.WriteLine("Содержимое очереди:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }
        }
    }
}
