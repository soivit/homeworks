﻿using System;
using System.Collections;

namespace Lesson1.Task1
{
    class ArrayListExample
    {
        public static void Show()
        {

            Console.WriteLine("Пример  ArrayList \n");
            ArrayList autors = new ArrayList();

            autors.Add("Lev Tolstoy");
            autors.Add("Aleksey Tolstoy");
            autors.Add("Anton Chehov");
            autors.Add("Ivan Turgenev");
            autors.Add("Mihail Bulgakov");
            
            Console.WriteLine("Цикл for");
            for (int i = 0; i < autors.Count; i++)
            {
                Console.WriteLine($"{autors[i]}");
            }
            Console.WriteLine("--------------------\n");

            Console.WriteLine("Цикл foreach");
            foreach (var autor in autors)
            {
                Console.WriteLine($"{autor}");
            }
        }
    }
}