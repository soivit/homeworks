﻿using System;
using System.Collections.Generic;

namespace Lesson1.Task1
{
    class GStackExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  Stack<T> \n");
            Stack<int> Example = new Stack<int>();
            Example.Push(1);
            Example.Push(2);
            Example.Push(3);
            Console.WriteLine("Содержимое стека:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }
           
            Console.WriteLine("\nУдалим элемент из стека");
            Example.Pop();
            Console.WriteLine("Содержимое стека:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }
           
            Console.WriteLine("\nДобавим элемент 77 в стек");
            Example.Push(77);
            Console.WriteLine("Содержимое стека:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nПросмотрим без удаления первый элемент в стеке");
            Console.WriteLine($"Значение первого элемента: {Example.Peek()}");
            Console.WriteLine("Содержимое стека:");
            foreach (var item in Example)
            {
                Console.WriteLine(item);
            }

        }
    }
}
