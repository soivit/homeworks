﻿using System;
using System.Collections.Generic;

namespace Lesson1.Task1
{
    class GLinkedListExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  LinkedList<T> \n");
            LinkedList<int> Example = new LinkedList<int>();
            
            Console.WriteLine("Вставка узла со значением 1");
            Example.AddLast(1);
            Console.WriteLine("Содержимое двухсвязного списка:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Вставка узла со значением 2 на первое место в списке");
            Example.AddFirst(2);
            Console.WriteLine("Содержимое двухсвязного списка:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Вставка узла со значением 3 в конец списка");
            Example.AddLast(3);
            Console.WriteLine("Содержимое двухсвязного списка:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Вставка узла со значением 4 перед элементом со значением 1");
            Example.AddBefore(Example.Find(1), 4);
            Console.WriteLine("Содержимое двухсвязного списка:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Удаление узла в начале и конце списка");
            Example.RemoveFirst();
            Example.RemoveLast();
            Console.WriteLine("Содержимое двухсвязного списка:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }
        }
    }
}
