﻿using System;
using System.Collections.Generic;

namespace Lesson1.Task1
{
    class GListExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  List<T> \n");
            List<int> Example = new List<int>() { 2, 4, 7, 3, 1 };

            Console.WriteLine("Содержимое списка:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nДобавление элемента со значением 55");
            Example.Add(55);
            Console.WriteLine("Содержимое списка:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nУдаление элемента по индексу (удалим элемент c индексом 2)");
            Example.RemoveAt(2);
            Console.WriteLine("Содержимое очереди:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nВставка элемента со значением 77 в позицию с индексом 3");
            Example.Insert(3, 77);
            Console.WriteLine("Содержимое очереди:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }
            
            Console.WriteLine("\nСортировка элементов");
            Example.Sort();
            Console.WriteLine("Содержимое очереди:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }
        }
    }
}
