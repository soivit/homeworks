﻿using System;
using System.Collections;

namespace Lesson1.Task1
{
    class HashTableExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  HashTable \n");
            Hashtable Example = new Hashtable();
            Example.Add("1", "Понедельник");
            Example.Add("2", "Вторник");
            Example.Add("3", "Среда");
            Example.Add("4", "Четверг");
            Example.Add("5", "Пятница");

            Console.WriteLine("Содержимое хэш таблицы:");
            foreach (DictionaryEntry item in Example)
            {
                Console.WriteLine($"Key = {item.Key}, Value = {item.Value}");
            }

            Console.WriteLine("\nДобавление элемента в хэш-таблицу Key=6, Value=Суббота");
            Example.Add("6", "Суббота");
            Console.WriteLine("Содержимое очереди:");
            foreach (DictionaryEntry item in Example)
            {
                Console.WriteLine($"Key = {item.Key}, Value = {item.Value}");
            }

            Console.WriteLine("\nУдаление элемента по ключу 5");
            Example.Remove("5");
            Console.WriteLine("Содержимое очереди:");
            foreach (DictionaryEntry item in Example)
            {
                Console.WriteLine($"Key = {item.Key}, Value = {item.Value}");
            }
           
        }
    }
}
