﻿using System;
using System.Collections;

namespace Lesson1.Task1
{
    class StackExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  Stack \n");
            Stack stackExample = new Stack();
            stackExample.Push(1);
            stackExample.Push(2);
            stackExample.Push(3);
            stackExample.Push(4);
            stackExample.Push(5);

            Console.WriteLine("Содержимое стека");
            foreach (var stack in stackExample)
            {
                Console.WriteLine($"{stack}");
            }
            Console.WriteLine("----------------\n");

            Console.WriteLine("Удалим элемент из стека");
            int str = (int)stackExample.Pop();
         
            foreach (var stack in stackExample)
            {
                Console.WriteLine(stack);
            }
            Console.WriteLine("----------------\n");

            Console.WriteLine("Добавим элемент в стек");
            stackExample.Push(77);
            foreach (var stack in stackExample)
            {
                Console.WriteLine(stack);
            }

                
        }
    }
}
