﻿using System;
using System.Collections.Generic;

namespace Lesson1.Task1
{
    class GHashSetExample
    {
        public static void Show()
        {
            Console.WriteLine("Пример  HashSet<T> \n");
            HashSet<int> Example = new HashSet<int> { 1, 2, 3 };
            Console.WriteLine("Содержимое множества:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nДобавление элемента 6 в множество");
            Example.Add(6);
            Console.WriteLine("Содержимое множества:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\nУдаление элемента 2");
            Example.Remove(2);
            Console.WriteLine("Содержимое множества:");
            foreach (int item in Example)
            {
                Console.WriteLine(item);
            }
        }
    }
}
