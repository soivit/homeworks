﻿using System;
using System.Collections.Generic;

namespace Lesson1.Task3
{
    class LinkedListExample
    {
        public static void Show()
        {
            LinkedList<string> autors = new LinkedList<string>();

            autors.AddFirst("Lev Tolstoy");
            autors.AddAfter(autors.Last, "Aleksey Tolstoy");
            autors.AddAfter(autors.Last, "Anton Chehov");
            autors.AddAfter(autors.Last, "Ivan Turgenev");
            autors.AddAfter(autors.Last, "Mihail Bulgakov");
            autors.AddAfter(autors.Last, "Ivan Ivanov");
            autors.AddAfter(autors.Last, "Victor Hugo");
            autors.AddAfter(autors.Last, "Jack London");
            autors.AddAfter(autors.Last, "Mark Twain");
            autors.AddLast("George Orwell");


            Console.WriteLine("--------------------");

            foreach (var autor in autors)
            {
                Console.WriteLine(autor);
            }

            Console.WriteLine("--------------------");

            Console.WriteLine("Удаление узла со значением Ivan Ivanov");
            autors.Remove("Ivan Ivanov");
            Console.WriteLine("Содержимое двухсвязного списка:");
            foreach (var item in autors)
            {
                Console.WriteLine(item);
            }

        }
    }
}
