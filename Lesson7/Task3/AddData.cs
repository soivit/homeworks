﻿using System;


namespace Lesson7.Task3
{
    public static class AddData
    {
        public static void AddAutorsToDb()
        {
            var dbContext = new AuthorsDbContext();
            var authors = AuthorsRepository.GetAutors();
            foreach (var author in authors)
            {
                dbContext.Authors.Add(author);
            }

            dbContext.SaveChanges();
            Console.WriteLine("Authors added");
        }
    }
}