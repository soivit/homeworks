﻿using System.Collections.Generic;
using Lesson7.Task2;

namespace Lesson7.Task3
{
    public static class AuthorsRepository 
    {
        public static List<Author> GetAutors()
        {
            var authors = new List<Author>();

            authors.Add(new Author() { FirstName = "Иван", LastName = "Иванов", Country = "Беларусь", Age = 43 });
            authors.Add(new Author() { FirstName = "Иван", LastName = "Сидоров", Country = "Россия", Age = 25 });
            authors.Add(new Author() { FirstName = "Евгений", LastName = "Соколов", Country = "Украина", Age = 27 });
            authors.Add(new Author() { FirstName = "Армен", LastName = "Джигарханян", Country = "Армения", Age = 29 });
            authors.Add(new Author() { FirstName = "Яков", LastName = "Рабинович", Country = "Израиль", Age = 42 });
            authors.Add(new Author() { FirstName = "Эмомали", LastName = "Рахмон", Country = "Такжикистан", Age = 29 });
            authors.Add(new Author() { FirstName = "Гурбангулы", LastName = "Бердымухамедов", Country = "Туркменистан", Age = 74 });
            authors.Add(new Author() { FirstName = "Агата", LastName = "Кристи", Country = "Великобритания", Age = 45 });
            authors.Add(new Author() { FirstName = "Петр", LastName = "Петров", Country = "Россия", Age = 42 });
            authors.Add(new Author() { FirstName = "Моисей", LastName = "Каган", Country = "Израиль", Age = 35 });

            return authors;
        }

    }
}