﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lesson5_2.Task2;

namespace Lesson5_2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Country> countries = new List<Country>();

            var country1 = new Country();
            country1.District.City.Neighborhood.StreetName = "Южная";
            country1.District.City.Neighborhood.HouseNumber = 25;
            countries.Add(country1);

            var country2 = new Country();
            country2.District.City.Neighborhood.StreetName = "Вишневая";
            country2.District.City.Neighborhood.HouseNumber = 10;
            countries.Add(country2);

            var country3 = new Country();
            country3.District.City.Neighborhood.StreetName = "Садовая";
            country3.District.City.Neighborhood.HouseNumber = 17;
            countries.Add(country3);
           
            var streets = countries.Select(c => c.District.City.Neighborhood.StreetName);
            foreach (var street in streets)
            {
                Console.WriteLine($"Улица: {street}");
            }
            Console.ReadKey();
        }
    }
}
