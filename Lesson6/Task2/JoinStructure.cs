﻿using System.Collections.Generic;

namespace Lesson6.Task2
{
    public struct JoinStructure
    {
        public string FirstName;
        public string LastName;
        public string StoryName;
    }
}
