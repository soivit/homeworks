﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson6.HelpMaterials;

namespace Lesson6.Task2
{
    class LeftJoin
    {
        public static void Show()
        {
            var characters = CharactersRepository.GetCharacters();
            var stories = StoriesRepository.GetStories();
          
            var result = from characater in characters
                         join story in stories
                         on characater.StoryId equals story.Id into characterStory
                         from story in characterStory.DefaultIfEmpty()
                         select new JoinStructure
                         {
                             FirstName = characater.FirstName,
                             LastName = characater.LastName,
                             StoryName = story == null ? "No story" : story.Name
                         };

            foreach (var item in result)
            {
                Console.WriteLine($"{item.FirstName}, {item.LastName}, {item.StoryName}");
            }

        }
    }
}   

