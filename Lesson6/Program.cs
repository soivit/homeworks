﻿using System;
using Lesson6.Task1;
using Lesson6.Task2;
using Lesson6.Task3;

namespace Lesson6
{
    class Program
    {
        static void Main(string[] args)
        {
            var taskToRun = Content.Task3_3;

            switch (taskToRun)
            {
                case Content.Task1:
                    LinqQuery.Show();
                    break;
                case Content.Task2:
                    LeftJoin.Show();
                    break;
                case Content.Task3_1:
                    DataBase.CreateTable();
                    break;
                case Content.Task3_2:
                    DataBase.InsertToTable();
                    break;
                case Content.Task3_3:
                    DataBase.ReadTable();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
    }
}
