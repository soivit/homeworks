﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;


namespace Lesson6.Task3
{
    public static class DataBase
    {
        public static void CreateTable()
        {
            var queryString =
               @"Create table Stories(Id int not null, Name varchar(50) not null, Description varchar(50))";
           
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["AdvanceCSharpCS"].ConnectionString;
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
        
        public static void InsertToTable()
        {
            var queryStringInsert =
            @"Insert into Stories values(1, 'Fairytail', 'none')";

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["AdvanceCSharpCS"].ConnectionString;
                SqlCommand command = new SqlCommand(queryStringInsert, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
        
        public static void ReadTable()
        {
            var queryStringRead = @"select * from Stories";

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["AdvanceCSharpCS"].ConnectionString;
                SqlCommand command = new SqlCommand(queryStringRead, connection);
                connection.Open();

                using (SqlDataReader sqlReader = command.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        Console.WriteLine($"Id: {sqlReader[0]}, \tName: {sqlReader[1]}," +
                            $"  \tDescription: {sqlReader[2]}");
                    }
                }
            }
        }
    }
  
}
