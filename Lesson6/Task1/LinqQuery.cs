﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson6.HelpMaterials;

namespace Lesson6.Task1
{
    class LinqQuery
    {
        public static void Show()
        {
            var characters = CharactersRepository.GetCharacters();
            var persons = characters.Select(character => new { PersonDescription  = character.FirstName 
                +" "+ character.LastName+" "+character.Age+" "+character.Gender});
            foreach(var person in persons)
            {
                Console.WriteLine(person.PersonDescription);
            }
        }
    }
}
