﻿using System.Collections;
using System.Collections.Generic;

namespace Lesson2.Task1
{
    public class UserCollection : IEnumerable
    {
        private readonly List<int> _list;

        public UserCollection(List<int> list)
        {
            _list = list;
        }

        public IEnumerator GetEnumerator()
        {
            return new Iterator(_list);
        }

        public void AddItem(int item)
        {
            _list.Add(item);
            foreach (var i in _list)
                System.Console.WriteLine(i);
        }

        public void RemoveItem(int item)
        {
            if (_list.Count > 5)
            {
                _list.Remove(item);
            }
            foreach (var i in _list)
                System.Console.WriteLine(i);
        }

    }

    public class Iterator : IEnumerator
    {
        private List<int> _list;
        private int _currentPosition = -1;

        public object Current
        {
            get
            {
                return _list[_currentPosition];
            }
        }

        public Iterator(List<int> list)
        {
            _list = list;
        }

        public bool MoveNext()
        {
            if (_currentPosition < _list.Count - 1)
            {
                _currentPosition++;
                return true;
            }

            return false;
        }

        public void Reset()
        {
            _currentPosition = -1;
        }

    }
}