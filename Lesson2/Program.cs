﻿using System;
using System.Collections.Generic;
using Lesson2.Task1;
using Lesson2.Task2;

namespace Lesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            var taskToRun = Content.Task2_2;

            var list = new List<int>() { 1, 2, 3, 45, 7, 15 };
            var ucol = new UserCollection(list);

            MyClass<int, MyType> myClass = new MyClass<int, MyType>();
            myClass.MyVar = 5;
            switch (taskToRun)
            {
                case Content.Task1_1:
                    ucol.AddItem(5);
                    break;
                case Content.Task1_2:
                    ucol.RemoveItem(3);
                    break;
                case Content.Task2_1:
                    Console.WriteLine(myClass.MyVar);
                    break;
                case Content.Task2_2:
                    Console.WriteLine(myClass.MyMethod(new MyType("123")));
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
    }
}
