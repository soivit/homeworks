﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2.Task2
{
    class MyClass <T, R>
    {
        public T MyVar { get; set; }
        public R MyMethod(R item)
        {
            return item;
        }

    }
}
